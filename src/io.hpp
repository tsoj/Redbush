#pragma once

#include <string>
#include <fstream>

namespace io
{
  inline std::string read_file(const std::string& file_path)
  {
    std::string ret = "";
    std::string tmp;
    std::ifstream file;
    file.open(file_path);
    if(!file.is_open())
    {
      throw std::runtime_error("Failed to open file: " + file_path);
    }
    while (std::getline(file, tmp))
    {
      ret += tmp + "\n";
    }
    file.close();
    return ret;
  }
}
