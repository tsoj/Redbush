/*!
  * \author Tsoj Tsoj
  * \version 0.1
  * \date 8-5-2019
  * \mainpage C++17 Entity-Component Library
  * \section intro_sec Introduction
  * This is a single header-file typesafe entity-component library for C++17. https://gitlab.com/tsoj/entity_component
*/
#pragma once

#include <cassert>
#include <vector>
#include <bitset>
#include <stdexcept>
#include <optional>

namespace entity_component
{
  /*!
    * \brief Manages all entities and components.
    * \details Due to limits of the language there can be only one entity_component::EntityManager at a time.
  */
  class EntityManager
  {
    #ifndef MAX_NUM_COMPONENT_TYPES
    static constexpr size_t MAX_NUM_COMPONENT_TYPES = 64;
    #endif

    template<typename T, typename... Targs>
    class Iterator;

  public:

    /*!
      * \brief Handle to an entity.
      * \details This class provides multiple functions to manage components attached to the corresponding entity.
    */
    class Entity
    {
      friend class EntityManager;

    public:

      /*!
        * \brief Creates and assigns a new component.
        * \tparam T type of the component that gets created
        * \param args constructor parameters
      */
      template<typename T, typename... Args>
      void create(Args&&... args)
      {
        if(components<T>.size() <= id)
        {
          components<T>.resize(id+1);
        }

        has_mask[id] |= bit_id<T>;

        components<T>[id] = T{std::forward<Args>(args)...};

        if(type_id<T> >= num_components.size())
        {
          num_components.resize(type_id<T>+1, 0);
        }
        num_components[type_id<T>] += 1;

        if(type_id<T> >= component_type_list.size())
        {
          component_type_list.resize(type_id<T>+1);
        }
        component_type_list[type_id<T>].push_back(*this);
      }

      /*!
        * \brief Removes a component from the entity.
        * \tparam T type of the component that gets removed
      */
      template<typename T>
      void remove()
      {
        assert(has<T>());
        has_mask[id] &= ~bit_id<T>;

        assert(type_id<T> < num_components.size());
        num_components[type_id<T>] -= 1;

        assert(type_id<T> < component_type_list.size());
        for(size_t i = 0; i<component_type_list[type_id<T>].size(); ++i)
        {
          if(component_type_list[type_id<T>][i].id == id)
          {
            component_type_list[type_id<T>].erase(component_type_list[type_id<T>].begin() + i);
            break;
          }
        }
      }

      /*!
        * \brief Returns a reference to the requested component.
        * \tparam T type of the component that gets requested
        * \return reference to the component that is assigned to this entity
        * \warning These references are unstable. They can get invalid.
        * \warning Undefined behaviour when requesting a component that hasn't been created for this entity.
      */
      template<typename T>
      T& get()
      {
        assert(has<T>() && components<T>[id].has_value());
        return *components<T>[id];
      }

      /*!
        * \brief Returns true when the entity has the specified components.
        * \tparam T,Targs... type of the requested component
        * \return true if entity has the component
      */
      template<typename T, typename... Targs>
      bool has() const
      {
        if(id >= has_mask.size())
        {
          return false;
        }
        return (has_mask[id] & bit_id<T, Targs...>) == bit_id<T, Targs...>;
      }

      size_t get_id()
      {
        return id;
      }

      inline bool operator==(const Entity& b)
      {
        return (this->id == b.id);
      }
      inline bool operator!=(const Entity& b)
      {
        return not ((*this) == b);
      }

    private:

      Entity(size_t id) : id(id) {}

      size_t id = -1;
    };

    /*!
      * \warning There can be only one instance of entity_component::EntityManager.
    */
    EntityManager()
    {
      if(instanced != 0)
      {
        throw std::runtime_error("Tried to instanciate multiple `EntityManager`.");
      }
      instanced = true;
    }
    ~EntityManager()
    {
      has_mask.resize(0);
      unused_ids.resize(0);
      num_components.resize(0);
      component_type_list.resize(0);
      instanced = false;
    }
    EntityManager(EntityManager const&) = delete;
    void operator=(EntityManager const&) = delete;

    /*!
      * \brief Creates a new entity.
      * \return handle to the created entity
    */
    Entity create_entity()
    {
      size_t id;
      if(unused_ids.empty())
      {
        id = has_mask.size();
        has_mask.emplace_back(1);
      }
      else
      {
        id = unused_ids.back();
        unused_ids.pop_back();
        assert(has_mask[id].none());
        has_mask[id] = std::bitset<MAX_NUM_COMPONENT_TYPES>(1);
      }
      assert(!(id >= has_mask.size()) && has_mask[id].test(0));
      return Entity(id);
    }

    /*!
      * \brief Removes a entity.
      * \param entity handle to the entity that should be removed
    */
    void remove_entity(const Entity& entity)
    {
      assert(!(entity.id >= has_mask.size()) && has_mask[entity.id].test(0)); // equivalent to assert(entity.has<void>())
      has_mask[entity.id] = std::bitset<MAX_NUM_COMPONENT_TYPES>(0);
      unused_ids.emplace_back(entity.id);
    }

    /*!
      * \brief Returns an iterator that can be used to iterate over specific entities.
      * \tparam T,Targs... type(s) of the component(s) that a entity should have to be included in the iterator
      * \details
      * To iterate over all entities that have the Components *A, B, ...* you need this for-loop:
      * @code
      *  for(auto entity : entityManager.iterator<A, B, ...>())
      *  {
      *    entity.get<A>() = ...;
      *  }
      * @endcode
    */
    template<typename T, typename... Targs>
    Iterator<T, Targs...> iterator()
    {
      return Iterator<T, Targs...>();
    }

    /*!
      * \brief Returns an iterator that can be used to iterate over specific entities.
      * \tparam T,Targs... type(s) of the component(s) that a entity should have to be included in the iterator
      * \param entity every entity that would come before this entity when using a normal iterator, will be skipped.
      * \details
      * Example of how to calculate gravitational forces. (use the operator++() on the iterator to skip the entity that has been given as parameter)
      * @code
      *  for(auto entity1 : entityManager.iterator<Position, Mass, Force>())
      *  {
      *    for(auto entity2 : ++entityManager.iterator<Position, Mass>(entity1))
      *    {
      *      Mass mass1 = entity1.get<Mass>();
      *      Mass mass2 = entity2.get<Mass>();
      *      Position position1 = entity1.get<Position>();
      *      Position position2 = entity2.get<Position>();
            entity1.get<Force>() += doGravityCalculations(mass1, mass2, position1, position2);
      *    }
      *  }
      * @endcode
    */
    template<typename T, typename... Targs>
    Iterator<T, Targs...> iterator(const Entity entity)
    {
      return Iterator<T, Targs...>(entity);
    }

  private:

    inline static bool instanced = false;

    template<size_t size_bit_mask>
    class TypeId
    {
    private:

      inline static std::bitset<size_bit_mask> get_unique_bit()
      {
        static std::bitset<size_bit_mask> counter = std::bitset<size_bit_mask>(0b10); // first bit is reserved for the `Entity` status
        std::bitset<size_bit_mask> ret = counter;
        counter <<= 1;
        if(counter.none())
        {
          throw std::runtime_error("Too many component types.");
        }
        return ret;
      }

      inline static size_t get_unique_index()
      {
       static size_t counter = 0;
       size_t ret = counter;
       counter += 1;
       return ret;
      }

      template<typename T>
      inline static std::bitset<size_bit_mask> get_bit_id_union()
      {
        static const std::bitset<size_bit_mask> unique_bit_id = get_unique_bit();
        return unique_bit_id;
      }
      template<typename T1, typename T2, typename... Targs>
      inline static std::bitset<size_bit_mask> get_bit_id_union()
      {
        return get_bit_id_union<T1>() | get_bit_id_union<T2, Targs...>();
      }

    public:

      template<typename T, typename... Targs>
      inline const static std::bitset<size_bit_mask> bit_id = get_bit_id_union<T, Targs...>();
      template<typename T>
      inline const static size_t id = get_unique_index();

    };

    template<typename T, typename... Targs>
    inline static const auto bit_id = TypeId<MAX_NUM_COMPONENT_TYPES>::bit_id<T, Targs...>;

    template<typename T>
    inline static const auto type_id = TypeId<MAX_NUM_COMPONENT_TYPES>::id<T>;

    template<typename T, typename... Targs>
    class Iterator
    {
    private:

      Entity entity;
      size_t i = 0;
      size_t rarest_component_id = get_rarest_component_id<T, Targs...>();
      Entity end_entity = Entity(has_mask.size());

      Entity next_valid_entity()
      {
        for(;rarest_component_id<component_type_list.size() && i<component_type_list[rarest_component_id].size(); ++i)
        {
          if(
            entity.id <= component_type_list[rarest_component_id][i].id &&
            component_type_list[rarest_component_id][i].template has<T, Targs...>()
          )
          {
            return component_type_list[rarest_component_id][i];
          }
        }
        return end_entity;
      }

    public:

      Iterator() : entity(0) {}
      Iterator(size_t id) : entity(id) {}
      Iterator(const Entity& entity) : entity(entity) {}

      Iterator<T, Targs...> begin()
      {
        return Iterator<T, Targs...>(next_valid_entity());
      }
      Iterator<T, Targs...> end()
      {
        return Iterator<T, Targs...>(end_entity);
      }

      bool operator== (const Iterator<T, Targs...>& a)
      {
        return this->entity == a.entity;
      }
      bool operator!= (const Iterator<T, Targs...>& a)
      {
        return !(*this == a);
      }

      Entity operator*()
      {
        return entity;
      }
      Iterator<T, Targs...> operator++() // Prefix Increment
      {
        entity.id += 1;
        i += 1;
        entity = next_valid_entity();
        return *this;
      }
    };

    inline static std::vector<size_t> unused_ids;

    template<typename T>
    inline static std::vector<std::optional<T>> components;
    inline static std::vector<std::bitset<MAX_NUM_COMPONENT_TYPES>> has_mask;

    inline static std::vector<size_t> num_components;
    inline static std::vector<std::vector<Entity>> component_type_list;


    template<typename T>
    static void get_rarest_component_id(size_t& current_id, size_t& current_min)
    {
      if(type_id<T> < num_components.size() && current_min > num_components[type_id<T>])
      {
        current_id = type_id<T>;
        current_min = num_components[type_id<T>];
      }
    }
    template<typename T1, typename T2, typename... Targs>
    static void get_rarest_component_id(size_t& current_id, size_t& current_min)
    {
      get_rarest_component_id<T1>(current_id, current_min);
      get_rarest_component_id<T2, Targs...>(current_id, current_min);
    }
    template<typename T, typename... Targs>
    static size_t get_rarest_component_id()
    {
      size_t current_id = SIZE_MAX;
      size_t current_min = SIZE_MAX;
      get_rarest_component_id<T, Targs...>(current_id, current_min);
      return current_id;
    }
  };

  template<>
  inline bool EntityManager::Entity::has<void>() const
  {
    if(id >= has_mask.size())
    {
      return false;
    }
    return has_mask[id].test(0);
  }

  template<>
  EntityManager::Entity EntityManager::Iterator<void>::next_valid_entity()
  {
    Entity i = entity;
    while(end().entity.id > i.id)
    {
      if(i.has<void>())
      {
        return i;
      }
      i.id+=1;
    }
    return end().entity;
  }
}
