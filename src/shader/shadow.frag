#version 330

#define NUM_CASCADES 4

layout (location = 0) out vec3 shadow;

in VsOut
{
  vec4 frag_position_light_space[NUM_CASCADES];
  vec4 frag_position_world_space;
} vs_out;

uniform sampler2DArrayShadow depth_map;
uniform float cascades[NUM_CASCADES];

vec4 directional_shadow_calculation(vec4 frag_position_light_space, int cascade)
{
  vec3 proj_coords = frag_position_light_space.xyz / frag_position_light_space.w;
  proj_coords = proj_coords * 0.5 + 0.5;
  float current_depth = proj_coords.z;
  return vec4(proj_coords.xy, cascade, current_depth);
}

void main()
{

  vec4 shadows_coords[NUM_CASCADES];
  vec2 d_x[NUM_CASCADES];
  vec2 d_y[NUM_CASCADES];
  for(int i = 0; i<NUM_CASCADES; i++)
  {
    shadows_coords[i] = directional_shadow_calculation(vs_out.frag_position_light_space[i], i);
    d_x[i] = dFdx(shadows_coords[i].xy);
    d_y[i] = dFdy(shadows_coords[i].xy);
  }

  shadow = vec3(0.9999999, 1.0, 1.0);
  //shadow = vec3(1.0, 1.0, 1.0);
  float current_cascade_distance = length(vs_out.frag_position_world_space.z);
  for(int i = 0; i<NUM_CASCADES; i++)
  {
    if(current_cascade_distance <= cascades[i])
    {
      shadow = vec3(1.0, 1.0, 1.0) * textureGrad(depth_map, shadows_coords[i], d_x[i], d_y[i]);
      break;
    }
  }



  /*//shadow = vec3(0.9999999, 1.0, 1.0);
  shadow = vec3(1.0, 1.0, 1.0);
  float current_cascade_distance = length(vs_out.frag_position_world_space.xyz);
  for(int i = 0; i<NUM_CASCADES; i++)
  {
    if(current_cascade_distance <= cascades[i])
    {
      shadow = vec3(1.0, 1.0, 1.0) * directional_shadow_calculation(
        vs_out.frag_position_light_space[i],
        depth_map,
        i
      );
      break;
    }
  }*/
}
