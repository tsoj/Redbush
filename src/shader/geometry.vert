#version 330

layout(location = 0) in vec3 vert_position_model_space;
layout(location = 1) in vec3 vert_normal_model_space;
layout(location = 2) in vec3 vert_tangent_model_space;
layout(location = 3) in vec2 texture_coordinate;

out VsOut
{
  vec3 frag_position_world_space;
  vec2 texture_coordinate;
  mat3 tangent_to_world_space;
} vs_out;

uniform mat4 model_to_world;
uniform mat4 world_to_projection;

void main()
{
  vec3 vert_normal_world_space = normalize(vec3(model_to_world * vec4(vert_normal_model_space, 0.0)));
  vec3 vert_tangent_world_space = normalize(vec3(model_to_world * vec4(vert_tangent_model_space, 0.0)));
  vec3 vert_bi_tangent_world_space = normalize(cross(vert_normal_world_space, vert_tangent_world_space));
  vs_out.tangent_to_world_space = inverse(transpose(mat3(
        vert_tangent_world_space,
        vert_bi_tangent_world_space,
        vert_normal_world_space
    )));

  vec3 vert_position_world_space = vec3(model_to_world * vec4(vert_position_model_space, 1.0));
  gl_Position = world_to_projection * vec4(vert_position_world_space, 1.0);
  vs_out.frag_position_world_space = vert_position_world_space;
  vs_out.texture_coordinate = texture_coordinate;
}
