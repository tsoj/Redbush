#version 330

layout(location = 0) in vec4 model_position;

uniform mat4 model_to_world;
uniform mat4 world_to_projection;

void main()
{
  vec3 world_position = vec3(model_to_world * model_position);
  gl_Position = world_to_projection * vec4(world_position, 1.0);
}
