#version 330

out vec4 out_color;

#define MAX_LIGHTS 5

in vec2 texture_coordinate;

uniform sampler2D position_texture;
uniform sampler2D diffuse_texture;
uniform sampler2D normal_texture;
uniform sampler2DArray shadow_texture;

uniform vec3 light_position[MAX_LIGHTS];
uniform vec3 light_color[MAX_LIGHTS];
uniform int num_lights;

void main()
{
  vec3 position = texture2D(position_texture, texture_coordinate).xyz;
  vec3 normal = texture2D(normal_texture, texture_coordinate).xyz;
  vec4 diffuse_color = texture2D(diffuse_texture, texture_coordinate).rgba;

  vec3 diffuse_light = vec3(0.0);
  for(int i = 0; i<num_lights; ++i)
  {
    vec3 shadow = texture(shadow_texture, vec3(texture_coordinate, i)).rgb;
    vec3 to_light = normalize(light_position[i] - position);
    diffuse_light = diffuse_light + clamp(dot(to_light, normal),0,1)*light_color[i]*shadow;
  }
  out_color = vec4(diffuse_light, 1.0)*diffuse_color;
}
