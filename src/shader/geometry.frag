#version 330

layout (location = 0) out vec3 gposition;
layout (location = 1) out vec3 gnormal;
layout (location = 2) out vec4 gdiffuse;

in VsOut
{
  vec3 frag_position_world_space;
  vec2 texture_coordinate;
  mat3 tangent_to_world_space;
} vs_out;

uniform sampler2D diffuse_texture;
uniform sampler2D normal_map;

void main()
{
  gposition = vs_out.frag_position_world_space;
  gnormal = vs_out.tangent_to_world_space*normalize(texture2D(normal_map, vs_out.texture_coordinate).rgb * 2.0 - vec3(1.0, 1.0, 1.0));
  gdiffuse = texture2D(diffuse_texture, vs_out.texture_coordinate);
}
