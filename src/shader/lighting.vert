#version 330

out vec2 texture_coordinate;

layout(location = 0) in vec2 position;
layout(location = 1) in vec2 tex_coords;

void main()
{
  gl_Position = vec4(position, 0.0, 1.0);
  texture_coordinate = tex_coords;
}
