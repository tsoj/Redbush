#version 330

#define NUM_CASCADES 4

layout(location = 0) in vec3 vert_position_model_space;
layout(location = 1) in vec3 vert_normal_model_space;
layout(location = 2) in vec3 vert_tangent_model_space;
layout(location = 3) in vec2 texture_coordinate;

out VsOut
{
  vec4 frag_position_light_space[NUM_CASCADES];
  vec4 frag_position_world_space;
} vs_out;

uniform mat4 model_to_world;
uniform mat4 world_to_projection;
uniform mat4 world_to_light[NUM_CASCADES];

void main()
{

  vec3 vert_position_world_space = vec3(model_to_world * vec4(vert_position_model_space, 1.0));
  gl_Position = world_to_projection * vec4(vert_position_world_space, 1.0);
  vs_out.frag_position_world_space = vec4(vert_position_world_space, 1.0);

  for(int i = 0; i<NUM_CASCADES; i++)
  {
    vs_out.frag_position_light_space[i] = world_to_light[i] * vec4(vert_position_world_space, 1.0);
  }
}
