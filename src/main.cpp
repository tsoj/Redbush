#include <iostream>
#include <chrono>

#include "Renderer.hpp"
#include "Obj.hpp"

std::vector<Renderable> get(const std::vector<obj::Object>& obj)
{
  std::vector<Renderable> ret;
  for(auto o : obj)
  {
    //TODO: do not load the same vertices and textures multiple times
    std::vector<Vertex> vertices = o.vertices;
    GL::VertexArray vao = GL::VertexArray(vertices);
    vao.set_vertex_attrib_pointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), offsetof(Vertex, position));
    vao.set_vertex_attrib_pointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), offsetof(Vertex, normal));
    vao.set_vertex_attrib_pointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), offsetof(Vertex, tangent));
    vao.set_vertex_attrib_pointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), offsetof(Vertex, texture_coordinate));
    GL::Texture2D texture = GL::Texture2D(o.mtl.diffuse_texture);
    GL::Texture2D normal_map = GL::Texture2D(o.mtl.normal_map);
    float biggest_vert_leng = 0.0;
    for(auto v : vertices)
    {
      if(glm::length(v.position) > biggest_vert_leng)
      {
        biggest_vert_leng = glm::length(v.position);
      }
    }
    ret.push_back(
      Renderable
      {
        texture,
        normal_map,
        vao,
        biggest_vert_leng
      }
    );
  }
  return ret;
}

struct SpaceBoat{};

int main()
{
  std::cout << "Hello" << std::endl;

  {
    Window w = Window(1900, 950, "Window");
    Renderer r = Renderer(w.width(), w.height());

    entity_component::EntityManager entity_manager;

    auto obj = obj::getObj("src/model/spaceboat.obj");

    auto spaceboat = entity_manager.create_entity();
    spaceboat.create<std::vector<Renderable>>(
      get(obj)
    );
    spaceboat.create<Position>(1000.0 - 40.0, 1000.0 - 8.0, 1000.0-5.0);
    spaceboat.create<Orientation>(1.0);
    spaceboat.create<SpaceBoat>();

    for(int i = 1; i<56; ++i)
    {
      auto spaceboat = entity_manager.create_entity();
      spaceboat.create<std::vector<Renderable>>(
        get(obj)
      );
      float x_plus = (i%8)*20.0;
      float y_plus = (i/8)*15.0;
      float z_plus = i*2.0;
      spaceboat.create<Position>(1000.0 - 40.0 + x_plus, 1000.0 - 8.0 + y_plus, 1000.0-5.0 - z_plus);//0.0, 0.0, -20.0);
      spaceboat.create<Orientation>(1.0);
      spaceboat.create<SpaceBoat>();
    }

    obj = obj::getObj("src/model/surface.obj");
    auto surface = entity_manager.create_entity();
    surface.create<std::vector<Renderable>>(
      get(obj)
    );
    surface.create<Position>(spaceboat.get<Position>() - glm::vec3(0.0, 20.0, 0.0));//1000.0, 1000.0 -5.0, 1000.0-20.0);//0.0, 0.0, -20.0);
    surface.create<Orientation>(1.0);
    surface.get<Orientation>() *= glm::rotate(glm::mat4(1), glm::radians(45.0f), glm::vec3(1.0, 0.0, 0.0));

    auto light0 = entity_manager.create_entity();
    light0.create<Position>(1000.0, 1000.0+50000.0, 1000.0-20.0);
    light0.create<Light>(glm::vec3(1.0, 1.0, 1.0));

    /*auto light1 = entity_manager.create_entity();
    light1.create<Position>(1000.0, 1000.0+50.0, 1000.0-20.0);
    light1.create<Light>(glm::vec3(0.0, 1.0, 0.0));*/

    /*auto light2 = entity_manager.create_entity();
    light2.create<Position>(1000.0 + 20.0, 1000.0+5000.0, 1000.0-30.0);
    light2.create<Light>(glm::vec3(1.0, 0.0, 0.0));

    auto light3 = entity_manager.create_entity();
    light3.create<Position>(1000.0 - 100.0, 1000.0+20.0, 1000.0);
    light3.create<Light>(glm::vec3(0.0, 0.0, 1.0));*/

    r.m_camera.position = Position(1000.0, 1000.0, 1000.0);

    auto last_time_point = std::chrono::steady_clock::now();
    while (!w.should_close())
    {
      auto current_time_point = std::chrono::steady_clock::now();
      float delta_seconds =
      std::chrono::duration_cast<std::chrono::nanoseconds>(current_time_point-last_time_point).count() * 0.000000001;
      last_time_point = current_time_point;

      r.render(entity_manager);
      w.set_title(std::to_string(r.get_frametime().count()/1000.0));
      w.draw_image(r.get_image());

      //r.m_camera.viewDirection =
      //glm::rotate(glm::mat4(1), glm::radians(5.0f*delta_seconds), glm::vec3(0.0, 1.0, 0.0))*
      //glm::vec4(r.m_camera.viewDirection, 1.0);
      //r.m_camera.position += glm::vec3(1.0, 0.0, 0.0)*delta_seconds;


      for(auto entity : entity_manager.iterator<SpaceBoat>())
      {
        entity.get<Orientation>() *= glm::rotate(glm::mat4(1), glm::radians(10.0f*delta_seconds), glm::vec3(1.0, 1.0, 1.0));
        entity.get<Position>().z += -2.5f*delta_seconds;
      }
      //spaceboat.get<Position>().z += -2.0f*delta_seconds;
      surface.create<Position>(spaceboat.get<Position>() - glm::vec3(0.0, 30.0, 0.0));
      /*int i = 0;
      for(auto entity : entity_manager.iterator<Orientation>())
      {
        i++;
        entity.get<Orientation>() *= glm::rotate(glm::mat4(1), glm::radians(5.0f*delta_seconds),
        glm::vec3(
          i%3, i%5, i%7
        )
        );
      }*/

      glfwPollEvents();
    }
  }

  std::cout << "\nGood bye" << std::endl;
}
