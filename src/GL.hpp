#pragma once

#include <string>
#include <initializer_list>
#include <map>
#include <vector>
#include <cassert>
#include <tuple>
#include <memory>
#include <iostream>

#include "io.hpp"
#include "lodepng/lodepng.hpp"
#include "types.hpp"
#include "utility.hpp"

#include <GL/glew.h>

namespace GL
{

  struct GLBufferTraits
  {
    static GLuint create()
    {
      GLuint id;
      glGenBuffers(1, &id);
      return id;
    }
    static void destroy(const GLuint id)
    {
      glDeleteBuffers(1, &id);
    }
  };
  struct GLProgramTraits
  {
    static GLuint create()
    {
      return glCreateProgram();
    }
    static void destroy(const GLuint id)
    {
      glDeleteProgram(id);
    }
  };
  struct GLVertexArrayTraits
  {
    static GLuint create()
    {
      GLuint id;
      glGenVertexArrays(1, &id);
      return id;
    }
    static void destroy(const GLuint id)
    {
      glDeleteVertexArrays(1, &id);
    }
  };
  struct GLTextureTraits
  {
    static GLuint create()
    {
      GLuint id;
      glGenTextures(1, &id);
      return id;
    }
    static void destroy(const GLuint id)
    {
      glDeleteTextures(1, &id);
    }
  };
  struct GLRenderbufferTraits
  {
    static GLuint create()
    {
      GLuint id;
      glGenRenderbuffers(1, &id);
      return id;
    }
    static void destroy(const GLuint id)
    {
      glDeleteRenderbuffers(1, &id);
    }
  };
  struct GLFramebufferTraits
  {
    static GLuint create()
    {
      GLuint id;
      glGenFramebuffers(1, &id);
      return id;
    }
    static void destroy(const GLuint id)
    {
      glDeleteFramebuffers(1, &id);
    }
  };

  template<typename T>
  class GLObject
  {
  public:
    GLObject() :
      m_copyCounter(new size_t(1)),
      m_id(T::create())
    {}
    ~GLObject()
    {
      destroy();
    }
    // copy constructor
    GLObject(const GLObject<T>& other) :
      m_copyCounter(other.m_copyCounter),
      m_id(other.m_id)
    {
      *m_copyCounter += 1;
    }
    // copy operator=
    GLObject<T>& operator=(const GLObject<T>& other)
    {
      destroy();
      m_id = other.m_id;
      m_copyCounter = other.m_copyCounter;
      *m_copyCounter += 1;
      return *this;
    }
    // move constructor
    GLObject(GLObject<T>&& other) = delete;
    // move operator=
    GLObject<T>& operator=(GLObject<T>&& other) = delete;

    GLuint id() const
    {
      return m_id;
    }

  private:

    void destroy()
    {
      assert(m_copyCounter != nullptr);
      assert(*m_copyCounter > 0);
      *m_copyCounter -= 1;
      if(*m_copyCounter == 0)
      {
        T::destroy(m_id);
        m_id = 0;
        delete m_copyCounter;
      }
    }

    size_t* m_copyCounter = nullptr;

    GLuint m_id = 0;
  };

  class Texture2D
  {
    friend class Framebuffer;
    friend class Program;
  public:
    Texture2D(
      const GLint internalformat,
      const GLsizei width,
      const GLsizei height,
      const GLenum format,
      const GLenum type
    )
    {
      assert(m_object.id() != 0);
      glBindTexture(GL_TEXTURE_2D, m_object.id());
      glTexImage2D(GL_TEXTURE_2D, 0, internalformat, width, height, 0, format, type, NULL);
    }
    Texture2D(const std::string& path)
    {
      assert(m_object.id() != 0);

      std::vector<unsigned char> image;
      unsigned int width, height;
      auto error = lodepng::decode(image, width, height, path);
      if(error)
      {
        throw std::runtime_error("Can't load image ["+std::string(path)+"]: decoder error " + std::to_string(error) + ": " + lodepng_error_text(error));
      }
      //TODO: don't duplicate code from exture2D(const std::vector<unsigned char>& image, const int width, const int height)
      glBindTexture(GL_TEXTURE_2D, m_object.id());
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image.data());
      //enable mipmapping
      glGenerateMipmap(GL_TEXTURE_2D);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, -0.3f);
      // anisotropic: TODO check if necessary
      /*float aniso = 0.0f;
      glBindTexture(GL_TEXTURE_2D, m_object.id());
      glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &aniso);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, aniso);*/
    }
    /*Texture2D(const std::vector<unsigned char>& image, const int width, const int height)
    {
      assert(m_object.id() != 0);

      glBindTexture(GL_TEXTURE_2D, m_object.id());
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image.data());
      //enable mipmapping
      glGenerateMipmap(GL_TEXTURE_2D);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, -0.3f);
    }*/
    Texture2D(){}

    void set_parameter(const GLenum pname, const GLint param)
    {
      glBindTexture(GL_TEXTURE_2D, m_object.id());
      glTexParameteri(GL_TEXTURE_2D, pname, param);
    }

  private:
    GLObject<GLTextureTraits> m_object;
  };

  class Texture2DArray
  {
    friend class Framebuffer;
    friend class Program;
  public:
    Texture2DArray(
      const GLenum internalformat,
      const GLsizei width,
      const GLsizei height,
      const GLsizei depth
    )
    {
      assert(m_object.id() != 0);
      glBindTexture(GL_TEXTURE_2D_ARRAY, m_object.id());
      glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, internalformat, width, height, depth);
    }
    Texture2DArray(){}

    void set_parameter(const GLenum pname, const GLint param)
    {
      glBindTexture(GL_TEXTURE_2D_ARRAY, m_object.id());
      glTexParameteri(GL_TEXTURE_2D_ARRAY, pname, param);
    }
    void set_parameter(const GLenum pname, const GLfloat param)
    {
      glBindTexture(GL_TEXTURE_2D_ARRAY, m_object.id());
      glTexParameterf(GL_TEXTURE_2D_ARRAY, pname, param);
    }

  private:
    GLObject<GLTextureTraits> m_object;
  };

  class Renderbuffer
  {
    friend class Framebuffer;
  public:
    Renderbuffer(const GLenum internalformat, const GLsizei width, const GLsizei height)
    {
      assert(m_object.id() != 0);
      glBindRenderbuffer(GL_RENDERBUFFER, m_object.id());
      glRenderbufferStorage(GL_RENDERBUFFER, internalformat, width, height);
    }
    Renderbuffer(){}

  private:
    GLObject<GLRenderbufferTraits> m_object;
  };

  class Framebuffer
  {
  public:
    Framebuffer()
    {
      assert(m_object.id() != 0);
      glBindFramebuffer(GL_FRAMEBUFFER, m_object.id());
      glDrawBuffer(GL_NONE);
      glReadBuffer(GL_NONE);
    }

    void attach(const Texture2D& texture2D, const GLenum attachment)
    {
      glBindFramebuffer(GL_FRAMEBUFFER, m_object.id());
      glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D, texture2D.m_object.id(), 0);
    }

    void attach(const Texture2DArray& texture2D_array, const GLenum attachment, const GLint layer)
    {
      glBindFramebuffer(GL_FRAMEBUFFER, m_object.id());
      glFramebufferTextureLayer(GL_FRAMEBUFFER, attachment, texture2D_array.m_object.id(), 0, layer);
    }

    void attach(const Renderbuffer& renderbuffer, const GLenum attachment)
    {
      glBindFramebuffer(GL_FRAMEBUFFER, m_object.id());
      glFramebufferRenderbuffer(GL_FRAMEBUFFER, attachment, GL_RENDERBUFFER, renderbuffer.m_object.id());
    }

    void set_draw_buffer(const std::initializer_list<GLenum>& attachment)
    {
      glBindFramebuffer(GL_FRAMEBUFFER, m_object.id());
      glDrawBuffers(attachment.size(), attachment.begin());
    }

    void bind()
    {
      glBindFramebuffer(GL_FRAMEBUFFER, m_object.id());
    }

  private:
    GLObject<GLFramebufferTraits> m_object;
  };

  class VertexArray
  {
    friend class Program;
  public:
    template<typename T>
    VertexArray(const std::vector<T>& vertices)
    {
      assert(m_object.id() != 0);
      assert(m_buffer_object.id() != 0);

      m_num_vertices = vertices.size();

      glBindVertexArray(m_object.id());

      glBindBuffer(GL_ARRAY_BUFFER, m_buffer_object.id());

      glBufferData(GL_ARRAY_BUFFER, sizeof(T) * vertices.size() , vertices.data(), GL_STATIC_DRAW);
    }
    VertexArray(){}

    void set_vertex_attrib_pointer(
      const GLuint index,
    	const GLint size,
    	const GLenum type,
    	const GLboolean normalized,
      const size_t size_of_type,
      const size_t offset
    )
    {
      glBindVertexArray(m_object.id());
      glEnableVertexAttribArray(index);
      glVertexAttribPointer(
        index,
        size,
        type,
        normalized,
        size_of_type,
        (void*)offset
      );
    }

    static VertexArray rectangle_vao()
    {
      static const std::vector<glm::vec2> vertices =
      {
        glm::vec2(-1.0, 1.0), glm::vec2(0.0, 1.0),
        glm::vec2(-1.0,-1.0), glm::vec2(0.0, 0.0),
        glm::vec2( 1.0,-1.0), glm::vec2(1.0, 0.0),

        glm::vec2(-1.0, 1.0), glm::vec2(0.0, 1.0),
        glm::vec2( 1.0,-1.0), glm::vec2(1.0, 0.0),
        glm::vec2( 1.0, 1.0), glm::vec2(1.0, 1.0)
      };
      static GL::VertexArray vao = GL::VertexArray(vertices);
      __attribute__((unused)) static const int nothing = (
      vao.set_vertex_attrib_pointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2)*2, 0),
      vao.set_vertex_attrib_pointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2)*2, sizeof(glm::vec2)), 0);
      return vao;
    }

  private:
    size_t m_num_vertices;
    GLObject<GLVertexArrayTraits> m_object;
    GLObject<GLBufferTraits> m_buffer_object;
  };

  class Program
  {

    static inline void inject_makros(
      std::string& shader_code,
      const std::initializer_list<std::string>& makro_list
    )
    {
      auto shader_lines = utility::split(shader_code, '\n');
      for(const auto& s : makro_list)
      {
        const auto makro = utility::split(s, ' ');
        if(
          makro.size() < 3 ||
          makro[0] != "#define"
        )
        {
          throw std::runtime_error("Tried to inject misformatted makro into shader: " + s);
        }

        bool found_makro = false;
        for(auto& line : shader_lines)
        {
          if(line.substr(0, 7) != "#define")
          {
            continue;
          }
          auto words = utility::split(line, ' ');
          if(words.size() < 3 || words[1] != makro[1])
          {
            continue;
          }
          line = s;
          found_makro = true;
          break;
        }
        if(!found_makro)
        {
          throw std::runtime_error("Can't find in shader: #define " + makro[1]);
        }
      }
      shader_code = "";
      for(const auto& line : shader_lines)
      {
        shader_code += line + "\n";
      }
    }

  public:

    Program(
      const std::initializer_list<std::tuple<std::string, GLenum, std::initializer_list<std::string>> >& shader_list
    )
    {
      assert(m_object.id() != 0);

    	std::vector<std::pair<std::string, GLuint>> shader_ids = std::vector<std::pair<std::string, GLuint>>();

    	for(const auto& shader : shader_list)
    	{
    		const GLuint shader_id = glCreateShader(std::get<1>(shader));
    		shader_ids.emplace_back(std::get<0>(shader), shader_id);
    		std::string shader_code = io::read_file(std::get<0>(shader));
        inject_makros(shader_code, std::get<2>(shader));
    		const char* adapter = shader_code.data();
    		glShaderSource(shader_id, 1, &adapter, 0);
    		glCompileShader(shader_id);
    	  glAttachShader(m_object.id(), shader_id);
    	}

      glLinkProgram(m_object.id());

    	std::string error = "";

    	for(const auto& shader : shader_ids)
    	{
    		GLint success = 0;
    	  glGetShaderiv(shader.second, GL_COMPILE_STATUS, &success);
    	  if(success == GL_FALSE)
    	  {
    	    GLint max_length = 0;
    		  glGetShaderiv(shader.second, GL_INFO_LOG_LENGTH, &max_length);
    	    std::vector<GLchar> error_log(max_length);

    	    glGetShaderInfoLog(shader.second, max_length, &max_length, &error_log[0]);

    	    error += "Failed to compile " + shader.first + ":\n" + std::string(error_log.data()) + "\n";
    	  }

    		glDetachShader(m_object.id(), shader.second);
    		glDeleteShader(shader.second);
    	}

    	GLint is_linked = 0;
    	glGetProgramiv(m_object.id(), GL_LINK_STATUS, (int*)&is_linked);
    	if(is_linked == GL_FALSE)
    	{
    		GLint max_length = 0;
    		glGetProgramiv(m_object.id(), GL_INFO_LOG_LENGTH, &max_length);
    		std::vector<GLchar> error_log(max_length);

    		glGetProgramInfoLog(m_object.id(), max_length, &max_length, &error_log[0]);

    		error += "Failed to link shader program:\n" + std::string(error_log.data()) + "\n";
    	}

    	if(error != "")
    	{
    		throw std::runtime_error("\n"+error);
    	}

    }
    Program(){}


    void uniform(const std::string& name, const glm::mat4& value)
    {
      use();
      glUniformMatrix4fv(get_uniform_location(name), 1, GL_FALSE, &value[0][0]);
    }

    void uniform(const std::string& name, const GLint& value)
    {
      use();
      glUniform1i(get_uniform_location(name), value);
    }

    void uniform(const std::string& name, const GLfloat& value)
    {
      use();
      glUniform1f(get_uniform_location(name), value);
    }

    void uniform(const std::string& name, const glm::vec3& value)
    {
      use();
      glUniform3fv(get_uniform_location(name), 1, &value[0]);
    }


    void uniform(const std::string& name, const GLsizei count, const glm::mat4* value)
    {
      use();
      glUniformMatrix4fv(get_uniform_location(name), count, GL_FALSE, &value[0][0][0]);
    }

    void uniform(const std::string& name, const GLsizei count, const GLint* value)
    {
      use();
      glUniform1iv(get_uniform_location(name), count, &value[0]);
    }

    void uniform(const std::string& name, const GLsizei count, const GLfloat* value)
    {
      use();
      glUniform1fv(get_uniform_location(name), count, &value[0]);
    }

    void uniform(const std::string& name, const GLsizei count, const glm::vec3* value)
    {
      use();
      glUniform3fv(get_uniform_location(name), count, &value[0][0]);
    }


    void uniform(const std::string& name, const Texture2D& texture2D)
    {
      use();
      uniform(name, m_texture_unit_counter);
      glActiveTexture(GL_TEXTURE0 + m_texture_unit_counter);
      glBindTexture(GL_TEXTURE_2D, texture2D.m_object.id());
      m_texture_unit_counter += 1;
    }

    void uniform(const std::string& name, const Texture2DArray& texture2D_array)
    {
      use();
      uniform(name, m_texture_unit_counter);
      glActiveTexture(GL_TEXTURE0 + m_texture_unit_counter);
      glBindTexture(GL_TEXTURE_2D_ARRAY, texture2D_array.m_object.id());
    }

    void draw(const VertexArray& vertexArray, const GLenum mode)
    {
      use();
      glBindVertexArray(vertexArray.m_object.id());
      glDrawArrays(mode, 0, vertexArray.m_num_vertices);
      m_texture_unit_counter = 0;
    }

  private:

    void use()
    {
      if(m_object.id() != s_active_program_id)
      {
        glUseProgram(m_object.id());
        s_active_program_id = m_object.id();
        m_texture_unit_counter = 0;
      }
    }

    GLint get_uniform_location(const std::string& name)
    {
      auto ret = m_uniform_locations.find(name);
      if(ret == m_uniform_locations.end())
      {
        m_uniform_locations[name] = glGetUniformLocation(m_object.id(), name.c_str());
        ret = m_uniform_locations.find(name);
      }
      assert(ret != m_uniform_locations.end());
      return ret->second;
    }

    std::map<std::string, GLint> m_uniform_locations;
    GLObject<GLProgramTraits> m_object;
    int m_texture_unit_counter = 0;
    inline static GLuint s_active_program_id = 0;
  };


  template<size_t i, size_t size, typename F, typename... T>
  void tuple_foreach_constexpr(const std::tuple<T...>& tuple, F func)
  {
    if constexpr(i<size)
    {
      func(std::get<i>(tuple));
      tuple_foreach_constexpr<i+1, size, F, T...>(tuple, func);
    }
  }
  template<typename F, typename... T>
  void tuple_foreach_constexpr(const std::tuple<T...>& tuple, F func)
  {
    tuple_foreach_constexpr<0, std::tuple_size<std::tuple<T...>>::value, F, T...>(tuple, func);
  }

  template <typename>
  struct is_tuple: std::false_type {};
  template <typename ...T>
  struct is_tuple<std::tuple<T...>>: std::true_type {};

  #define typeof(expression) typename std::remove_const<typename std::remove_reference<decltype(expression)>::type>::type

  template<typename T_MeshData, typename... T_GeneralUniforms, typename... T_SpecificUniforms>
  void render_pass
  (
    const std::vector<T_MeshData>& meshs,
    const GL::VertexArray T_MeshData::*vao,
    GL::Framebuffer* render_target,
    const GLint viewport_x, const GLint viewport_y,
    const GLsizei viewport_width, const GLsizei viewport_height,
    const GLbitfield clear_bits,
    const GLenum mode,
    GL::Program& program,
    const std::tuple<T_GeneralUniforms...>& general_uniforms,
    const std::tuple<T_SpecificUniforms...>& specific_uniforms
  )
  {
    //TODO viewport
    glViewport(viewport_x, viewport_y, viewport_width, viewport_height);
    if(render_target == nullptr)
    {
      glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }
    else
    {
      render_target->bind();
    }
    glClear(clear_bits);

    tuple_foreach_constexpr(general_uniforms, [&program](const auto &x)
    {
      static_assert(is_tuple<typeof(x)>::value, "expecting a std::tuple of size 2 or 3");
      static_assert(std::tuple_size<typeof(x)>::value == 2 || std::tuple_size<typeof(x)>::value == 3,
      "tuple needs to have 2 or 3 elements");
      if constexpr(std::tuple_size<typeof(x)>::value == 3)
      {
          program.uniform(std::get<0>(x), std::get<1>(x), std::get<2>(x));
      }
      else if constexpr(std::is_pointer<typeof(std::get<1>(x))>::value)
      {
        program.uniform(std::get<0>(x), *std::get<1>(x));
      }
      else
      {
        program.uniform(std::get<0>(x), std::get<1>(x));
      }

    });

    for(const T_MeshData& mesh : meshs)
    {
      tuple_foreach_constexpr(specific_uniforms, [&program, &mesh](const auto &x)
      {
        static_assert(is_tuple<typeof(x)>::value, "expecting a std::tuple of size 2 or 3");
        static_assert(std::tuple_size<typeof(x)>::value == 2 || std::tuple_size<typeof(x)>::value == 3,
        "tuple needs to have 2 or 3 elements");
        if constexpr(std::tuple_size<typeof(x)>::value == 3)
        {
            program.uniform(std::get<0>(x), std::get<1>(x), mesh.*std::get<2>(x));
        }
        else if constexpr(std::is_pointer<typeof(mesh.*std::get<1>(x))>::value)
        {
          program.uniform(std::get<0>(x), *(mesh.*std::get<1>(x)));
        }
        else
        {
          program.uniform(std::get<0>(x), mesh.*std::get<1>(x));
        }
      });

      program.draw(mesh.*vao, mode);
    }
  }

  #undef typeof

  void check_for_errors()
  {
    std::string error_message = "";
    GLenum error;
    do
    {
      error = glGetError();
      switch(error)
      {
        case GL_INVALID_ENUM: error_message += "GL_INVALID_ENUM\n"; break;
        case GL_INVALID_VALUE: error_message += "GL_INVALID_VALUE\n"; break;
        case GL_INVALID_OPERATION: error_message += "GL_INVALID_OPERATION\n"; break;
        case GL_INVALID_FRAMEBUFFER_OPERATION: error_message += "GL_INVALID_FRAMEBUFFER_OPERATION\n"; break;
        case GL_OUT_OF_MEMORY: error_message += "GL_OUT_OF_MEMORY\n"; break;
        case GL_STACK_UNDERFLOW: error_message += "GL_STACK_UNDERFLOW\n"; break;
        case GL_STACK_OVERFLOW: error_message += "GL_STACK_OVERFLOW\n"; break;
      }
    } while(error != GL_NO_ERROR);
    if(!error_message.empty())
    {
      std::cout << error_message << std::flush;
    }
  }

}
