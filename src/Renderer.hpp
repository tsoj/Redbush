#pragma once

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <iostream>
#include <algorithm>
#include <chrono>

#include "GL.hpp"
#include "Window.hpp"
#include "entity_component.hpp"
#include "utility.hpp"

//TODO: switch to eigen?

struct Renderable
{
  GL::Texture2D texture;
  GL::Texture2D normal_map;
  GL::VertexArray vao;
  float biggest_vert_leng;
};

class Renderer
{

public:
  Renderer(int width, int height) : m_width(width), m_height(height)
  {
    m_camera.aspect_ratio = (float)m_width/(float)m_height;

    m_depth_buffer = GL::Renderbuffer(GL_DEPTH_COMPONENT32F, m_width, m_height);

    // position color buffer
    m_gposition = GL::Texture2D(GL_RGB16F, m_width, m_height, GL_RGB, GL_FLOAT);
    m_gposition.set_parameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    m_gposition.set_parameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    m_gposition.set_parameter(GL_TEXTURE_WRAP_S, GL_REPEAT);
    m_gposition.set_parameter(GL_TEXTURE_WRAP_T, GL_REPEAT);

    // normal buffer
    m_gnormal = GL::Texture2D(GL_RGB16F, m_width, m_height, GL_RGB, GL_FLOAT);
    m_gnormal.set_parameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    m_gnormal.set_parameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    m_gnormal.set_parameter(GL_TEXTURE_WRAP_S, GL_REPEAT);
    m_gnormal.set_parameter(GL_TEXTURE_WRAP_T, GL_REPEAT);

    // color VertexBuffer
    m_gdiffuse = GL::Texture2D(GL_RGBA, m_width, m_height, GL_RGBA, GL_UNSIGNED_BYTE);
    m_gdiffuse.set_parameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    m_gdiffuse.set_parameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    m_gdiffuse.set_parameter(GL_TEXTURE_WRAP_S, GL_REPEAT);
    m_gdiffuse.set_parameter(GL_TEXTURE_WRAP_T, GL_REPEAT);

    // create gBuffer
    m_gbuffer = GL::Framebuffer();
    m_gbuffer.attach(m_gposition, GL_COLOR_ATTACHMENT0);
    m_gbuffer.attach(m_gnormal, GL_COLOR_ATTACHMENT1);
    m_gbuffer.attach(m_gdiffuse, GL_COLOR_ATTACHMENT2);
    m_gbuffer.attach(m_depth_buffer, GL_DEPTH_ATTACHMENT);
    m_gbuffer.set_draw_buffer({GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2});

    // target image
    m_target_image = GL::Texture2D(GL_RGBA, m_width, m_height, GL_RGBA, GL_UNSIGNED_BYTE);
    m_target_image.set_parameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    m_target_image.set_parameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    m_target_image.set_parameter(GL_TEXTURE_WRAP_S, GL_REPEAT);
    m_target_image.set_parameter(GL_TEXTURE_WRAP_T, GL_REPEAT);

    m_target_buffer = GL::Framebuffer();
    m_target_buffer.attach(m_target_image, GL_COLOR_ATTACHMENT0);
    m_target_buffer.attach(m_depth_buffer, GL_DEPTH_ATTACHMENT);
    m_target_buffer.set_draw_buffer({GL_COLOR_ATTACHMENT0});

    // create depthMap
    m_depth_map = GL::Texture2DArray(GL_DEPTH_COMPONENT32F, m_depth_map_size, m_depth_map_size, m_cascades.size()-1);
    m_depth_map.set_parameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    m_depth_map.set_parameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    m_depth_map.set_parameter(GL_TEXTURE_WRAP_S, GL_REPEAT);
    m_depth_map.set_parameter(GL_TEXTURE_WRAP_T, GL_REPEAT);
    m_depth_map.set_parameter(GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
    m_depth_map.set_parameter(GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);

    m_depth_map_buffer.resize(m_cascades.size()-1);
    for(size_t i = 0; i<m_cascades.size()-1; ++i)
    {
      // create depthMap buffer
      m_depth_map_buffer[i].attach(m_depth_map, GL_DEPTH_ATTACHMENT, i);
    }

    // create shadow buffer

    m_shadow = GL::Texture2DArray(GL_RGBA8, m_width, m_height, m_max_lights);
    m_shadow.set_parameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    m_shadow.set_parameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    m_shadow.set_parameter(GL_TEXTURE_WRAP_S, GL_REPEAT);
    m_shadow.set_parameter(GL_TEXTURE_WRAP_T, GL_REPEAT);
    m_shadow.set_parameter(GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
    m_shadow.set_parameter(GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);

    m_shadow_buffer.resize(m_max_lights);
    for(size_t i = 0; i<m_max_lights; ++i)
    {
      m_shadow_buffer[i].attach(m_shadow, GL_COLOR_ATTACHMENT0, i);
      m_shadow_buffer[i].attach(m_depth_buffer, GL_DEPTH_ATTACHMENT);
      m_shadow_buffer[i].set_draw_buffer({GL_COLOR_ATTACHMENT0});
    }
  }

  void render(entity_component::EntityManager& entity_manager)
  {
    auto start = std::chrono::high_resolution_clock::now();

    glClearColor(0.0, 0.0, 0.0, 1.0);

    /*geometry rendering pass*/
    fill_mesh_data(m_mesh_data, m_camera, -m_camera.position, entity_manager);
    glm::mat4 world_to_projection = m_camera.projection(-m_camera.position);
    GL::render_pass(
      m_mesh_data,
      &MeshData::vao,
      &m_gbuffer, 0, 0, m_width, m_height,
      GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT,
      GL_TRIANGLES,
      m_geometry_program,
      std::make_tuple
      (
        std::tuple("world_to_projection", world_to_projection)
      ),
      std::make_tuple
      (
        std::tuple("model_to_world", &MeshData::model_to_world),
        std::tuple("diffuse_texture", &MeshData::texture),
        std::tuple("normal_map", &MeshData::normal_map)
      )
    );
    //TODO: 1. solve situation with being in camera space or world space
    //TODO: 2. use structs to pass arguments to large functions (render_pass, etc.)
    //TODO: 3. support instanced meshs
    //TODO: 4. dynamic far and near plane


    m_light_positions.clear();
    m_light_colors.clear();
    for(auto e : entity_manager.iterator<Light, Position>())
    {
      m_light_positions.push_back(e.get<Position>()-m_camera.position);
      m_light_colors.push_back(e.get<Light>().color);
      if(m_light_positions.size() == m_max_lights)
      {
        break;
      }
    }

    /*shadow generation pass*/
    for(size_t i = 0; i<m_light_positions.size(); ++i)
    {
      /*depth map generation*/
      std::vector<glm::mat4> world_to_light_projection(m_cascades.size()-1);
      for(size_t cascade = 0; cascade<m_cascades.size()-1; ++cascade)
      {
        Camera cascade_camera = m_camera;
        cascade_camera.near_plane = m_cascades[cascade];
        cascade_camera.far_plane = m_cascades[cascade+1];
        Camera depth_map_camera = get_depth_map_projection_data(cascade_camera, m_light_positions[i] + m_camera.position);
        if(!(depth_map_camera.field_of_view_degrees > 180.0f || depth_map_camera.field_of_view_degrees < 0.0f) )
        {
          fill_mesh_data(m_mesh_data, depth_map_camera, -m_camera.position, entity_manager);//TODO: fix view frustum calling here, wrong camera offset
          world_to_light_projection[cascade] = depth_map_camera.projection(-m_camera.position);

          glCullFace(GL_BACK);
          GL::render_pass(
            m_mesh_data,
            &MeshData::vao,
            &m_depth_map_buffer[cascade],
            0, 0, m_depth_map_size, m_depth_map_size,
            GL_DEPTH_BUFFER_BIT,
            GL_TRIANGLES,
            m_depth_map_program,
            std::make_tuple
            (
              std::tuple("world_to_projection", world_to_light_projection[cascade])
            ),
            std::make_tuple
            (
              std::tuple("model_to_world", &MeshData::model_to_world)
            )
          );
          glCullFace(GL_FRONT);
        }
      }
      /*shadow generation pass*/
      fill_mesh_data(m_mesh_data, m_camera, -m_camera.position, entity_manager);
      glm::mat4 world_to_projection = m_camera.projection(-m_camera.position);
      GL::render_pass(
        m_mesh_data,
        &MeshData::vao,
        &m_shadow_buffer[i], 0, 0, m_width, m_height,
        GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT,
        GL_TRIANGLES,
        m_shadow_program,
        std::make_tuple
        (
          std::tuple("world_to_projection", world_to_projection),
          std::tuple("world_to_light[0]", m_cascades.size()-1, world_to_light_projection.data()),
          std::tuple("cascades[0]", m_cascades.size()-1, &m_cascades[1]),
          std::tuple("depth_map", m_depth_map)
        ),
        std::make_tuple
        (
          std::tuple("model_to_world", &MeshData::model_to_world)
        )
      );
    }


    /*lighting rendering pass*/
    struct MeshData
    {
      GL::VertexArray vao;
    } mesh_data {
      GL::VertexArray::rectangle_vao()
    };

    GL::render_pass(
      {mesh_data},
      &MeshData::vao,
      &m_target_buffer, 0, 0, m_width, m_height,
      GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT,
      GL_TRIANGLES,
      m_lighting_program,
      std::make_tuple(
        std::tuple("light_position[0]", m_light_positions.size(), m_light_positions.data()),
        std::tuple("light_color[0]", m_light_positions.size(), m_light_colors.data()),
        std::tuple("num_lights", (GLint)m_light_positions.size()),
        std::tuple("position_texture", m_gposition),
        std::tuple("diffuse_texture", m_gdiffuse),
        std::tuple("normal_texture", m_gnormal),
        std::tuple("shadow_texture", m_shadow)
      ),
      std::make_tuple()
    );
    GL::check_for_errors();
    auto end = std::chrono::high_resolution_clock::now();
    m_frametime = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
  }

  struct Camera
  {
    glm::vec3 view_direction;
    glm::vec3 up;
    Position position;
    float field_of_view_degrees;
    float aspect_ratio;
    float near_plane, far_plane;

    glm::mat4 projection(const Position& camera_position_offset = Position(0.0, 0.0, 0.0)) const
    {
      return
        glm::perspective(
          glm::radians(field_of_view_degrees),
          aspect_ratio,
          near_plane, far_plane) *
        glm::lookAt(position + camera_position_offset, (position + camera_position_offset) + view_direction, up);
    }
    bool is_sphere_visible(const Position& sphere_position, const float sphere_radius) const
    {
      glm::vec3 normal[6];
      Position base[6];

      glm::vec3 h_v = glm::normalize(view_direction);
      glm::vec3 w_v = glm::normalize(glm::cross(up, view_direction));
      glm::vec3 u_v = glm::normalize(up);

      float u_a = glm::tan(glm::radians(field_of_view_degrees/2.0));
      float w_a = u_a*aspect_ratio;

      //near plane
      normal[0] = -h_v;
      base[0] = position + near_plane*h_v;

      //far plane
      normal[1] = h_v;
      base[1] = position + far_plane*h_v;

      //up plane
      normal[2] = glm::cross(h_v + u_a*u_v, w_v);
      base[2] = position;

      //bottom plane
      normal[3] = glm::cross(w_v, h_v - u_a*u_v);
      base[3] = position;

      //left plane
      normal[4] = glm::cross(up, h_v + w_a*w_v);
      base[4] = position;

      //right plane
      normal[5] = glm::cross(h_v - w_a*w_v, up);
      base[5] = position;

      for(int i = 0; i<6; ++i)
      {
        if(utility::distance_to_plane(
          sphere_position + sphere_radius*glm::normalize(-normal[i]),
          normal[i],
          base[i]
        ) >= 0.0)
        {
          return false;
        }
      }

      return true;

    }

    void get_points_of_view_frustum(Position view_frustum_coords[8]) const
    {
      glm::vec3 h_v = glm::normalize(view_direction);
      glm::vec3 w_v = glm::normalize(glm::cross(up, view_direction));
      glm::vec3 u_v = glm::normalize(glm::cross(w_v, view_direction));

      float h_a = near_plane;
      float u_a = glm::tan(glm::radians(field_of_view_degrees)/2.0)*h_a;
      float w_a = u_a*aspect_ratio;

      float h_a_m = far_plane;
      float u_a_m = glm::tan(glm::radians(field_of_view_degrees)/2.0)*h_a_m;
      float w_a_m = u_a_m*aspect_ratio;

      view_frustum_coords[0] = position + h_v*h_a_m + u_v*u_a_m + w_v*w_a_m;
      view_frustum_coords[1] = position + h_v*h_a_m - u_v*u_a_m + w_v*w_a_m;
      view_frustum_coords[2] = position + h_v*h_a_m + u_v*u_a_m - w_v*w_a_m;
      view_frustum_coords[3] = position + h_v*h_a_m - u_v*u_a_m - w_v*w_a_m;
      view_frustum_coords[4] = position + h_v*h_a + u_v*u_a + w_v*w_a;
      view_frustum_coords[5] = position + h_v*h_a - u_v*u_a + w_v*w_a;
      view_frustum_coords[6] = position + h_v*h_a + u_v*u_a - w_v*w_a;
      view_frustum_coords[7] = position + h_v*h_a - u_v*u_a - w_v*w_a;
    }
  };
  Camera m_camera = Camera
  {
    glm::vec3(0.0, 0.0, -1.0),
    glm::vec3(0.0, 1.0, 0.0),
    Position(0.0, 0.0, 0.0),
    60.0f,
    1.0,
    0.1f, 1000.0f
  };

  GL::Texture2D get_image()
  {
    return m_target_image;//m_shadow[0];
  }

  std::chrono::microseconds get_frametime()
  {
    return m_frametime;
  }

private:
  const std::vector<float> m_cascades = {0.0, 100.0, 200.0, 500.0, 1000.0};
  const size_t m_max_lights = 5;
  const size_t m_depth_map_size = 1024;
  const int m_width, m_height;

  std::chrono::microseconds m_frametime;

  struct MeshData
  {
    const GL::Texture2D texture;
    const GL::Texture2D normal_map;
    const GL::VertexArray vao;
    const glm::mat4 model_to_world;
  };

  std::vector<MeshData> m_mesh_data;

  inline static void fill_mesh_data(
    std::vector<MeshData>& mesh_data,
    const Camera& camera,
    const glm::vec3& offset,
    entity_component::EntityManager& entity_manager
  )
  {
    mesh_data.clear();
    for(auto e : entity_manager.iterator<std::vector<Renderable>, Position, Orientation>())
    {
      const glm::mat4 model_to_world = glm::translate(glm::mat4(1), (e.get<Position>() + offset)) * e.get<Orientation>();
      for(const auto& r : e.get<std::vector<Renderable>>())
      {
        // skipping objects, that are not in the view frustum.
        if(!camera.is_sphere_visible(e.get<Position>(), r.biggest_vert_leng))
        {
          continue;
        }
        mesh_data.push_back(MeshData{r.texture, r.normal_map, r.vao, model_to_world});
      }
    }
  }

  GL::Renderbuffer m_depth_buffer;

  GL::Framebuffer m_gbuffer;
  GL::Texture2D m_gposition;
  GL::Texture2D m_gdiffuse;
  GL::Texture2D m_gnormal;

  std::vector<GL::Framebuffer> m_depth_map_buffer;
  GL::Texture2DArray m_depth_map;

  std::vector<GL::Framebuffer> m_shadow_buffer;
  GL::Texture2DArray m_shadow;

  GL::Framebuffer m_target_buffer;
  GL::Texture2D m_target_image;

  std::vector<Position> m_light_positions;
  std::vector<Position> m_light_colors;

  GL::Program m_lighting_program = GL::Program({
    {"src/shader/lighting.vert",GL_VERTEX_SHADER, {}},
    {
      "src/shader/lighting.frag",GL_FRAGMENT_SHADER,
      {"#define MAX_LIGHTS "+std::to_string(m_max_lights)}
    }
  });
  GL::Program m_geometry_program = GL::Program({
    {"src/shader/geometry.vert",GL_VERTEX_SHADER, {}},
    {"src/shader/geometry.frag",GL_FRAGMENT_SHADER, {}}
  });
  GL::Program m_depth_map_program = GL::Program({
    {"src/shader/depth_map.vert", GL_VERTEX_SHADER, {}},
    {"src/shader/depth_map.frag", GL_FRAGMENT_SHADER, {}}
  });
  GL::Program m_shadow_program = GL::Program({
    {
      "src/shader/shadow.vert", GL_VERTEX_SHADER,
      {"#define NUM_CASCADES "+std::to_string(m_cascades.size()-1)}
    },
    {
      "src/shader/shadow.frag", GL_FRAGMENT_SHADER,
      {"#define NUM_CASCADES "+std::to_string(m_cascades.size()-1)}
    }
  });


  static inline Camera get_depth_map_projection_data(
    const Camera& camera,
    const Position& light_position
  )
  {

    Position view_frustum_coords[8];
    camera.get_points_of_view_frustum(view_frustum_coords);

    Camera ret;
    ret.view_direction = glm::normalize(
      view_frustum_coords[0]+
      view_frustum_coords[1]+
      view_frustum_coords[2]+
      view_frustum_coords[3]+
      view_frustum_coords[4]+
      view_frustum_coords[5]+
      view_frustum_coords[6]+
      view_frustum_coords[7]-
      light_position*8.0f
    );
    ret.up = glm::vec3(-ret.view_direction.y, ret.view_direction.x, ret.view_direction.z);//TODO
    float biggest_angle = 0.0;
    ret.far_plane = 0.0;
    // TODO: fix ret.near_plane
    constexpr float max_shadow_distance = 2000.0;
    for(size_t i = 0; i<8; i++)
    {
      float current_angle =
        glm::acos(
          glm::abs(glm::dot(ret.view_direction, view_frustum_coords[i] - light_position))/
          (glm::length(ret.view_direction)*glm::length(view_frustum_coords[i] - light_position))
        );
      if(current_angle > biggest_angle)
      {
        biggest_angle = current_angle;
      }
      ret.far_plane = std::max<float>(ret.far_plane, glm::distance(light_position, (glm::vec3)view_frustum_coords[i]));
    }
    ret.near_plane = std::max<float>(ret.far_plane - max_shadow_distance, 1.0);
    ret.field_of_view_degrees = glm::degrees(2.0*biggest_angle);
    ret.aspect_ratio = 1.0;//TODO: optimize this
    ret.position = light_position;
    return ret;
  }

  static inline Camera get_depth_map_projection_data_2(
    const Camera& camera,
    const Position& light_position
  )
  {
    //TODO: create new design with directional far-away-lights and spot-lights with predefined projection data
  }
};
