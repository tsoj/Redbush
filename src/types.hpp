
#pragma once

#include <glm/glm.hpp>

#define strong_typedef(base, new)\
struct new : base\
{\
  template<typename... Targs>\
  new(const Targs&... args) : base(args...){}\
};

struct Vertex
{
  glm::vec3 position;
  glm::vec3 normal;
  glm::vec3 tangent;
  glm::vec2 texture_coordinate;
};

struct Light
{
  glm::vec3 color;
};

strong_typedef(glm::vec3, Position);

strong_typedef(glm::mat4, Orientation);

#undef strong_typedef
