#pragma once

#include "GL.hpp"
#include "types.hpp"

#include <GLFW/glfw3.h>
#include <utility>

class Window
{
public:
  Window(int w, int h, const char* title)
  {
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    m_glfw_window = glfwCreateWindow(w, h, title, NULL, NULL);
    if(!m_glfw_window)
    {
      glfwTerminate();
      throw std::runtime_error("Failed to initialize Window or context.\n");
    }
    glfwMakeContextCurrent(m_glfw_window);

    GLenum err = glewInit();
    if (GLEW_OK != err)
    {
      throw std::runtime_error("Failed to initialize GLEW: "+ std::string((char*)glewGetErrorString(err)) + "\n");
    }

    glfwSwapInterval(1);

    int width, height;
    glfwGetFramebufferSize(m_glfw_window, &width, &height);
    glViewport(0, 0, width, height);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT);
    glFrontFace(GL_CW);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
  }
  ~Window()
  {
    glfwDestroyWindow(m_glfw_window);
  }

  Window(const Window&) = delete;
  Window& operator=(const Window&) = delete;

  void make_context_current()
  {
    glfwMakeContextCurrent(m_glfw_window);
  }
  void swap_buffers()
  {
    glfwSwapBuffers(m_glfw_window);
  }
  bool should_close() const
  {
    return glfwWindowShouldClose(m_glfw_window);
  }
  void set_title(const std::string& title)
  {
    glfwSetWindowTitle(m_glfw_window, title.c_str());
  }

  int width() const
  {
    int width, height;
    glfwGetFramebufferSize(m_glfw_window, &width, &height);
    return width;
  }
  int height() const
  {
    int width, height;
    glfwGetFramebufferSize(m_glfw_window, &width, &height);
    return height;
  }

  void draw_image(const GL::Texture2D& image)
  {
    static GL::Program program = GL::Program({
      {"src/shader/image.vert",GL_VERTEX_SHADER, {}},
      {"src/shader/image.frag",GL_FRAGMENT_SHADER, {}}
    });

    struct MeshData
    {
      GL::VertexArray vao;
      GL::Texture2D image;
    } mesh_data = {GL::VertexArray::rectangle_vao(), image};

    GL::render_pass(
      {mesh_data},
      &MeshData::vao,
      nullptr, 0, 0, width(), height(),
      GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT,
      GL_TRIANGLES,
      program,
      std::make_tuple(),
      std::make_tuple
      (
        std::tuple("image", &MeshData::image)
      )
    );

    swap_buffers();
  }

private:
  GLFWwindow* m_glfw_window = nullptr;

  struct RAII_InitGLFW
  {
    RAII_InitGLFW()
    {
      glfwSetErrorCallback(
        [](int error, const char* description)
        {
          throw std::runtime_error("Error: " + std::string(description) + " (" + std::to_string(error) + ")\n");
        }
      );

      if(!glfwInit())
      {
        throw std::runtime_error("Failed to initialize GLFW.\n");
      }
    }
    ~RAII_InitGLFW()
    {
      glfwTerminate();
    }
  };
  inline static const RAII_InitGLFW init_glfw = RAII_InitGLFW();
};
