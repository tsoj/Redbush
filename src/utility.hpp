#pragma once

#include <glm/glm.hpp>
#include <string>
#include <sstream>
#include <vector>

namespace utility
{
  float distance_to_plane(const Position& point, const glm::vec3& normal, const glm::vec3& base)
  {
    return (
      normal.x*point.x +
      normal.y*point.y +
      normal.z*point.z +
      -normal.x*base.x +
      -normal.y*base.y +
      -normal.z*base.z
    ) / glm::length(normal);
  }

  std::vector<std::string> split(const std::string& s, char delim)
	{
		std::vector<std::string> elements;
		std::stringstream ss(s);
		std::string item;
		while (std::getline(ss, item, delim))
		{
			if (item != "")
			{
				elements.push_back(item);
			}
		}
		return elements;
	}

}
