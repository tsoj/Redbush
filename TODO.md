- different custom shaders:
  - including volumetric stuff, procedural terrain/planets, PBR, sceleton animation, ...
- shadows:
  - shadows for spotlights and directional lights
  - custom shadows e.g. for volumetric stuff
  - ambient occlusion
- reflections:
  - basic cube map of generell environment (e.g. skybox)
  - screen space relfelctions
- instanced rendering
- internal frustum culling
- solution for z-buffer precision, far-,nearplane (large scale scenes)


api idea:

submit_light(...);
submit_shadow_sun(...);
submit_shadow_spotlight(...);
submit_pbr_object(vertex_data, textures, position, orientation, optional_geometry_pass_shader);
submit_forward_render_object(shader, vertex_data, T... extra_data);
